#!/bin/bash -ex
cd "$(eval "$script_dir_eval")/.."
script_dir="$(pwd)"
retry="retry 5"
################################################################################
#params
[ -z "$tf_ver" ] && tf_ver="$1"
[ -z "$tf_ver" ] && tf_ver=r2.0
################################################################################
#deps
LBLUE echo "tf deps"
script-helper-are-all-installed libprotobuf-c-dev protobuf-compiler

LBLUE echo "bazel deps"
script-helper-are-all-installed pkg-config zip g++ zlib1g-dev unzip python
################################################################################
#vars
[ -z "$base_dir"    ] && base_dir="$script_dir/$tf_ver"
[ -z "$install_dir" ] && install_dir="$base_dir/install"
[ -z "$conda_dir"   ] && conda_dir="$base_dir/conda"
export NEWLINE=$'\n'
jobs=$(($(nproc)/2))

#tf vars
tf_dir="$base_dir/tensorflow"
tf_cfg="$tf_dir/.tf_configure.bazelrc"
tf_branch="origin/$tf_ver"
tf_pip_pkg_dir="$base_dir/tensorflow-pip-pkg"

#bazel vars
[ "$tf_branch" == "origin/r2.0"  ] && bazel_ver="0.26.1"
[ "$tf_branch" == "origin/r1.13" ] && bazel_ver="0.21.0"

[ -z "$bazel_ver" ] && RED errcho "found no bazel version for tf = '$tf_branch'" exit 1

[ -z "$keep_bazel_file"   ] && keep_bazel_file=false
bazel_file_name="bazel-$bazel_ver-installer-linux-x86_64.sh"
bazel_file="$base_dir/$bazel_file_name"
bazel_url="https://github.com/bazelbuild/bazel/releases/download/$bazel_ver/$bazel_file_name"
################################################################################
#cfg
LGREEN
echo "config
    base
        base_dir        : $base_dir
        install_dir     : $install_dir
        conda_dir       : $conda_dir
    tf
        tf_ver          : $tf_ver
        tf_dir          : $tf_dir
        tf_branch       : $tf_branch
    bazel
        bazel_ver       : $bazel_ver
        bazel_file      : $bazel_file
        bazel_url       : $bazel_url
        keep_bazel_file : $keep_bazel_file
"
NOCOLOR
################################################################################
#base setup
mkdir -p "$install_dir"
cd "$base_dir"
ev-export-install-directory "$install_dir"
################################################################################
#conda
if ! [ "$(readlink -f "$conda_dir")" == "$(stupidconda-env-location)" ]
then
    stupidconda-activate-miniconda-inline
    [ -e "$conda_dir" ] || conda create -y --prefix "$conda_dir" pip
    conda activate "$conda_dir"
fi
################################################################################
#bazel
if ! which bazel
then
    LBLUE echo "installing bazel"
    [ -e "$bazel_file" ] || $retry wget "$bazel_url" --output-document="$bazel_file"
    chmod +x "$bazel_file"
    "$bazel_file" --prefix="$install_dir"
    $keep_bazel_file || rm "$bazel_file"
else
    LBLUE echo "bazel is already installed"
fi
################################################################################
#tf
LBLUE echo "installing tensorflow pip dependencies"
pip install -U pip six numpy wheel setuptools mock
pip install -U keras_applications==1.0.6 --no-deps
pip install -U keras_preprocessing==1.0.5 --no-deps

LBLUE echo "building tensorflow"
[ -e "$tf_dir" ] || $retry git clone                \
    https://github.com/tensorflow/tensorflow.git    \
    "$tf_dir"
cd "$tf_dir"
git checkout --track "$tf_branch" || true
if [ -e "$tf_cfg" ]
then
    LBLUE echo "tensorflow already configured"
else
    LBLUE echo "configuring tensorflow"
    yes "$NEWLINE" | ./configure
fi
LBLUE echo "compiling tensorflow"
$retry bazel build --config=opt                                        \
            "//tensorflow/tools/pip_package:build_pip_package"  \
            --jobs=$jobs                                        \
            --verbose_failures
LBLUE echo "building tensorflow python package"
$retry "$tf_dir/bazel-bin/tensorflow/tools/pip_package/build_pip_package" "$tf_pip_pkg_dir"
LBLUE echo "installing tensorflow python package"
pip install "$tf_pip_pkg_dir"/*

